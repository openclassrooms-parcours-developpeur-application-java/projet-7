# PROJET 7

PROJET 7 - DEVELOPPEZ LE NOUVEAU SYSTEME D’INFORMATION DE LA BIBLIOTHEQUE D’UNE GRANDE VILLE

**CONTEXTE**

Le service culturel d’une grande ville souhaite moderniser la gestion de ses bibliothèques. Pour cela, elle désire mettre à disposition de ses usagers, un système de suivi des prêts de leurs ouvrages.

Ce système comprendra :

    • un site web (en responsive design) accessible aux usagers et permettant :
        o de rechercher des ouvrages et voir le nombre d’exemplaires disponibles
        o de suivre leurs prêts en cours. Les prêts sont pour une période de 4 semaines (durée configurable)
        o de prolonger un prêt. Le prêt d’un ouvrage n’est prolongeable qu’une seule fois. La prolongation ajoute une nouvelle période de prêt (4 semaines, durée de prolongation configurable) à la période initiale
    • une application mobile iOS et Android fournissant les mêmes services que le site web
    • une application spécifique pour le personnel des bibliothèque permettant, entre autres, de gérer les emprunts et le livres rendus
    • un batch lancé régulièrement et qui enverra des mails de relance aux usagers n’ayant pas rendu les livres en fin de période de prêt

À vous de réaliser ce système !

Ce projet va être réalisé en plusieurs itérations. Vous ne réaliserez que la première (se référer au paragraphe « travail demandé » pour le détail de cette itération).
Afin de centraliser les règles de gestion et généraliser les développements, vous adopterez une démarche SOA (Service Oriented Architecture – Architecture Orientée Services). Vous prévoyez de réaliser un web service qui portera la majeure partie de la logique métier et les applications s’appuieront sur celui-ci.

**Contraintes**

Le déploiement du système sera assuré par le personnel de la direction des systèmes d'information de la ville. Vous devez donc leur laisser la possibilité de modifier facilement les différents paramètres de configuration (URL et identifiant/mot de passe de la base de données, URL du webservice, envoi des mails...)

**TRAVAIL DEMANDE**

Vous devez réaliser :

    • Une base de données PostgreSQL.
    • Un web service SOAP permettant :
        o l’identification des usagers via un identifiant et un mot de passe,
        o de remonter les disponibilités des ouvrages,
        o de gérer les prêts (nouveau prêt, prolongation, retour de prêt, état des prêts en cours / prolongés / non rendus à temps...)
    • Une application web, basée sur le framework Apache Struts 2, servant d’interface pour les utilisateurs. Elle ne doit pas se connecter à la base de données, tout passe par le web-service.
    • Un batch qui envoie un mail de relance aux usagers n’ayant pas rendu les ouvrages dont la période de prêt est terminée. Il ne doit pas se connecter à la base de données, tout passe par le web service.
    • Une documentation générale (une dizaine de pages) décrivant la solution fonctionnelle et technique mise en place et sa mise en œuvre (configuration, déploiement). Ce document comprendra :
        o un diagramme de classes UML décrivant le domaine fonctionnel
        o les principales règles de gestion
        o le modèle physique de données (MPD). Si besoin, il peut faire l’objet d’un document PDF à part
        o La solution technique mise en place
        o La mise en œuvre du système (configuration, déploiement).
    • Une documentation succincte expliquant comment construire les images docker depuis les sources et déployer l’application. Un fichierREADME.mdsuffit. Le mentor doit être en mesure de déployer le système d’information chez lui pour le tester avant la soutenance.

Vous n’avez pas à développer les applications mobiles et le système des postes internes aux bibliothèques.
Vous réaliserez le web service, l'application web et le batch en Java/JEE (JDK 8) avec les fonctionnalités décrites ci-dessus. Le web service et l'application web seront déployés sur un serveur Apache Tomcat 9.

Le code sera géré avec Git et ce, dès le début du projet. Vous aurez donc un historique assez important sur votre dépôt Git.

Votre mentor et le mentor de soutenance pourront consulter cet historique dans le but, non pas de relever vos erreurs, mais plutôt de voir votre démarche de développement et votre utilisation de Git.
N'ayez donc pas peur de faire des commits ! Au contraire, un historique contenant seulement quelques "gros" commits révèle souvent une mauvaise approche de développement et une utilisation peu efficace de Git.

Les différents composants seront packagés avec Maven :

    • le web service : au format WAR, et un ZIP des fichiers de configuration nécessaires
    • l’application web ouverte aux usagers : au format WAR, et un ZIP des fichiers de configuration nécessaires
    • le batch : au format JAR exécutable ainsi qu'un fichier ZIP contenant le JAR exécutable, les dépendances Java nécessaires, les fichiers de configuration, un script shell (sh ou bash) de lancement

**LIVRABLES ATTENDUS**

Vous livrerez, sur GitHub ou GitLab (dans un seul dépôt Git dédié) :

    • Le code source de l’application
    • Les scripts SQL de création de la base de données avec un jeu de données de démo
    • Les fichiers de configuration exemple
    • Le WSDL du web service ainsi qu’un projet SoapUI contenant un plan de test du web-service
    • Les ressources nécessaires à la création des images docker des différents composants du système (base de données, web service, application web, batch) ainsi que les fichiers de configuration de docker-compose permettant de déployer :
        o le webservice (base de données + web-service)
        o l’application web (base de données + web-service + application web)
        o le batch (base de données + web-service + batch)
    • La documentation générale du projet au format PDF
    • Une documentation succincte (un fichier  README.md  suffit) expliquant comment construire les images docker depuis les sources et déployer l'application

